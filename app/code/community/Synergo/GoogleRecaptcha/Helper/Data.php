<?php
/**
 * @category   Synergo
 * @package    Synergo_GoogleRecaptcha
 * @author     Synergo
 * @website    http://www.synergo.id
 * @license
 *
 * Google reCAPTCHA Helper
 */
class Synergo_GoogleRecaptcha_Helper_Data extends Mage_Captcha_Helper_Data
{
    /**
     * Google type captcha
     */
    const LIBRARY_GOOGLE_RECAPTCHA = 'recaptcha';

    /**
     * Zend type captcha
     */
    const LIBRARY_ZEND             = 'zend';

    /**
     * Light theme Google reCAPTCHA
     */
    const THEME_LIGHT              = 'light';

    /**
     * Dark theme Google reCAPTCHA
     */
    const THEME_DARK               = 'dark';

    /**
     * Google reCAPTCHA JavaScript resource
     */
    const RECAPTCHA_JS_PATH        = 'https://www.google.com/recaptcha/api.js';

    /**
     * Google reCAPTCHA verify path
     */
    const RECAPTCHA_VERIFY_PATH    = 'https://www.google.com/recaptcha/api/siteverify';
}