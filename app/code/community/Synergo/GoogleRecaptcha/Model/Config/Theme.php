<?php
/**
 * @category   Synergo
 * @package    Synergo_GoogleRecaptcha
 * @author     Synergo
 * @website    http://www.synergo.id
 * @license
 *
 * Google reCAPTCHA Theme
 */
class Synergo_GoogleRecaptcha_Model_Config_Theme
{
    /**
     * Get options for captcha theme selection field
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('synergo_googlerecaptcha');
        return array(
            array(
                'label' => $helper->__('Light'),
                'value' => Synergo_GoogleRecaptcha_Helper_Data::THEME_LIGHT
            ),
            array(
                'label' => $helper->__('Dark'),
                'value' => Synergo_GoogleRecaptcha_Helper_Data::THEME_DARK
            ),
        );
    }
}