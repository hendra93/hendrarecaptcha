<?php
/**
 * @category   Synergo
 * @package    Synergo_GoogleRecaptcha
 * @author     Synergo
 * @website    http://www.synergo.id
 * @license
 *
 * Google reCAPTCHA Language
 */
class Synergo_GoogleRecaptcha_Model_Config_Language
{
    /**
     * Get options for captcha language selection field
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('synergo_googlerecaptcha');
        return array(
            array(
                'label' => $helper->__('Arabic'),
                'value' => 'ar'
            ),
            array(
                'label' => $helper->__('Bulgarian'),
                'value' => 'bg'
            ),
            array(
                'label' => $helper->__('Catalan'),
                'value' => 'ca'
            ),
            array(
                'label' => $helper->__('Chinese (Simplified)'),
                'value' => 'zh-CN'
            ),
            array(
                'label' => $helper->__('Chinese (Traditional)'),
                'value' => 'zh-TW'
            ),
            array(
                'label' => $helper->__('Croatian'),
                'value' => 'hr'
            ),
            array(
                'label' => $helper->__('Czech'),
                'value' => 'cs'
            ),
            array(
                'label' => $helper->__('Danish'),
                'value' => 'da'
            ),
            array(
                'label' => $helper->__('Dutch'),
                'value' => 'nl'
            ),
            array(
                'label' => $helper->__('English (UK)'),
                'value' => 'en-GB'
            ),
            array(
                'label' => $helper->__('English (US)'),
                'value' => 'en'
            ),
            array(
                'label' => $helper->__('Filipino'),
                'value' => 'fil'
            ),
            array(
                'label' => $helper->__('Finnish'),
                'value' => 'fi'
            ),
            array(
                'label' => $helper->__('French'),
                'value' => 'fr'
            ),
            array(
                'label' => $helper->__('French (Canadian)'),
                'value' => 'fr-CA'
            ),
            array(
                'label' => $helper->__('German'),
                'value' => 'de'
            ),
            array(
                'label' => $helper->__('German (Austria)'),
                'value' => 'de-AT'
            ),
            array(
                'label' => $helper->__('German (Switzerland)'),
                'value' => 'de-CH'
            ),
            array(
                'label' => $helper->__('Greek'),
                'value' => 'el'
            ),
            array(
                'label' => $helper->__('Hebrew'),
                'value' => 'iw'
            ),
            array(
                'label' => $helper->__('Hindi'),
                'value' => 'hi'
            ),
            array(
                'label' => $helper->__('Hungarain'),
                'value' => 'hu'
            ),
            array(
                'label' => $helper->__('Indonesian'),
                'value' => 'id'
            ),
            array(
                'label' => $helper->__('Italian'),
                'value' => 'it'
            ),
            array(
                'label' => $helper->__('Japanese'),
                'value' => 'ja'
            ),
            array(
                'label' => $helper->__('Korean'),
                'value' => 'ko'
            ),
            array(
                'label' => $helper->__('Latvian'),
                'value' => 'lv'
            ),
            array(
                'label' => $helper->__('Lithuanian'),
                'value' => 'lt'
            ),
            array(
                'label' => $helper->__('Norwegian'),
                'value' => 'no'
            ),
            array(
                'label' => $helper->__('Persian'),
                'value' => 'fa'
            ),
            array(
                'label' => $helper->__('Polish'),
                'value' => 'pl'
            ),
            array(
                'label' => $helper->__('Portuguese'),
                'value' => 'pt'
            ),
            array(
                'label' => $helper->__('Portuguese (Brazil)'),
                'value' => 'pt-BR'
            ),
            array(
                'label' => $helper->__('Portuguese (Portugal)'),
                'value' => 'pt-PT'
            ),
            array(
                'label' => $helper->__('Romanian'),
                'value' => 'ro'
            ),
            array(
                'label' => $helper->__('Russian'),
                'value' => 'ru'
            ),
            array(
                'label' => $helper->__('Serbian'),
                'value' => 'sr'
            ),
            array(
                'label' => $helper->__('Slovak'),
                'value' => 'sk'
            ),
            array(
                'label' => $helper->__('Slovenian'),
                'value' => 'sl'
            ),
            array(
                'label' => $helper->__('Spanish'),
                'value' => 'es'
            ),
            array(
                'label' => $helper->__('Spanish (Latin America)'),
                'value' => 'es-419'
            ),
            array(
                'label' => $helper->__('Swedish'),
                'value' => 'sv'
            ),
            array(
                'label' => $helper->__('Thai'),
                'value' => 'th'
            ),
            array(
                'label' => $helper->__('Turkish'),
                'value' => 'tr'
            ),
            array(
                'label' => $helper->__('Ukrainian'),
                'value' => 'uk'
            ),
            array(
                'label' => $helper->__('Vietnamese'),
                'value' => 'vi'
            )
        );
    }
}