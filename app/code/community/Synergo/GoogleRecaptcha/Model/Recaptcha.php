<?php
/**
 * @category   Synergo
 * @package    Synergo_GoogleRecaptcha
 * @author     Synergo
 * @website    http://www.synergo.id
 * @license
 *
 * Google reCAPTCHA Model
 */
class Synergo_GoogleRecaptcha_Model_Recaptcha extends Mage_Captcha_Model_Zend
{
    /**
     * Get Block Name
     *
     * @return string
     */
    public function getBlockName()
    {
        return 'synergo_googlerecaptcha/captcha_recaptcha';
    }

    /**
     * Returns captcha helper
     *
     * @return Synergo_GoogleRecaptcha_Helper_Data
     */
    protected function _getHelper()
    {
        if (empty($this->_helper)) {
            $this->_helper = Mage::helper('synergo_googlerecaptcha');
        }
        return $this->_helper;
    }

    /**
     * Verify reCAPTCHA
     * @param  string  $word
     * @return boolean
     */
    public function isCorrect($word)
    {
        $request = Mage::app()->getRequest();
        if ($request) {
            try {
                $url = Synergo_GoogleRecaptcha_Helper_Data::RECAPTCHA_VERIFY_PATH
                        . "?secret=" . $this->_getSecretKey()
                        . "&response=" . $request->getParam('g-recaptcha-response');
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_TIMEOUT, 15);
                $response =curl_exec($curl);
                curl_close($curl);

                $response = json_decode($response, true);
                return ($response['success'] === true) ? true : false;
            } catch (Exception $e) {
                Mage::log($e->getMessage());
            }
        }
        return false;
    }

    /**
     * Return Google reCAPTCHA JavaScript Resource
     * @return string
     */
    public function getUrl()
    {
        $url = Synergo_GoogleRecaptcha_Helper_Data::RECAPTCHA_JS_PATH;
        if ( ! empty($this->_getLanguage())) {
            $url .= "?hl=" . $this->_getLanguage();
        }
        return (string) $url;
    }

    /**
     * Get Google reCAPTCHA site key
     * @return string
     */
    public function getSiteKey()
    {
        return (string) $this->_getHelper()->getConfigNode('site_key');
    }

    /**
     * Retrieve Google reCAPTCHA secret key
     * @return string
     */
    protected function _getSecretKey()
    {
        return (string) $this->_getHelper()->getConfigNode('secret_key');
    }

    /**
     * Get Google reCAPTCHA theme
     * @return string
     */
    public function getTheme()
    {
        return (string) $this->_getHelper()->getConfigNode('theme');
    }

    /**
     * Get Google reCAPTCHA language
     * @return string
     */
    protected function _getLanguage()
    {
        return (string) $this->_getHelper()->getConfigNode('language');
    }

    /**
     * Get Google reCAPTCHA tabindex
     * @return int
     */
    public function getTabindex()
    {
        return (string) $this->_getHelper()->getConfigNode('tabindex');
    }
}