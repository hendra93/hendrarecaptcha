<?php
/**
 * @category   Synergo
 * @package    Synergo_GoogleRecaptcha
 * @author     Synergo
 * @website    http://www.synergo.id
 * @license
 *
 * Google reCAPTCHA Block
 */
class Synergo_GoogleRecaptcha_Block_Captcha_Recaptcha extends Mage_Captcha_Block_Captcha_Zend
{
    /**
     * Google reCAPTCHA template
     * @var string
     */
    protected $_template = 'synergo/googlerecaptcha/recaptcha.phtml';
}