GoogleRecaptcha Community Extension
=====================
By Synergo

Facts
-----
- version: 1.0.0
- extension key: Synergo_GoogleRecaptcha
- [extension on Bitbucket](https://bitbucket.org/synergoteam/googlerecaptcha)

Description
-----------
Allow users to replace Zend Captcha with the new Google reCAPTCHA

Requirements
------------
- PHP >= 5.4.0
- Mage_Captcha

Compatibility
-------------
- Magento >= 1.9.1

Installation Instructions
-------------------------
1. Copy all extension files to your Magento installation.
2. Clear the cache, logout from the admin panel and then login again.
3. Configure and activate the extension under System - Configuration - Customer Configuration - CAPTCHA for frontend CAPTCHA or System - Configuration - Admin - CAPTCHA for admin CAPTCHA.

Uninstallation
--------------
Remove all extension files from your Magento installation.

Support
-------
If you have any issues with this extension, open an issue on [Bitbucket](https://bitbucket.org/synergoteam/googlerecaptcha/issues).

Contribution
------------
Any contribution is highly appreciated. The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/synergoteam/googlerecaptcha/pull-requests).

Developer
---------
Synergo
[http://www.synergo.id](http://www.synergo.id)
[@synergoID](https://twitter.com/synergoID)

Licence
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

Copyright
---------
(c) 2015 Synergo GoogleRecaptcha
